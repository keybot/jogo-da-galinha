extends Node2D

onready var npc = preload("res://scenes/Npc.tscn")

signal timeout
signal timeout2

var score = 0
var level = 20
var negative_gap = -2560 * level
var instances = []


func _ready():
	$HUD/ScoreBox/HBoxContainer/Label.text = "Pontos:"
	$HUD/ScoreBox/HBoxContainer/Score.text = str(score)
	add_npc()


func add_npc():
	for i in range(level):
		instances.append(npc.instance())
		instances[i].position = Vector2((rand_range(-2560, 2560) + (5120 * i) + negative_gap), 500)
		instances[i].connect("Npc_salvo", self, "rescue")
		call_deferred("add_child", instances[i])


func rescue():
	score += 5
	$HUD/ScoreBox/HBoxContainer/Score.text = str(score)


func _on_Chicken_Start_timer():
	$Chicken/Timer.start()


func _on_Timer_timeout():
	$Chicken/Timer.stop()
	var npcs = []
	var npcs_positions = []
	var i = 0
	
	for element in instances:
		if is_instance_valid(element):
			npcs.append(element)
			npcs_positions.append(element.position)
		i += 1
	instances = npcs
	
	emit_signal("timeout", $Player.position, npcs_positions)


func _on_Chicken_Start_timer2():
	$Chicken/Timer2.start()


func _on_Timer2_timeout():
	$Chicken/Timer2.stop()
	emit_signal("timeout2")


func _on_Chicken_Em_risco():
	if !$Chicken/Timer2.is_stopped():
		get_tree().reload_current_scene()

