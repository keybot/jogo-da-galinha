extends KinematicBody2D

var delta_fix = 0

var speed = 40

var velocity = Vector2()

func _ready():
	pass

func get_input():
	var desenho = get_node("AnimatedSprite")
	
	look_at(get_global_mouse_position())
	velocity = Vector2()
	if Input.is_action_pressed("click"):
		velocity = Vector2(speed, 0).rotated(rotation)
		desenho.play("movendo")
		if(abs(velocity.angle()) < PI/2):
			desenho.set_flip_v(false)
		else:
			desenho.set_flip_v(true)
	else:
		desenho.stop()

func _physics_process(delta):
	delta_fix = 0
	get_input()
	velocity = move_and_slide(velocity / delta)
