extends Area2D

var delta_fix = 0

signal Npc_salvo
signal Npc_morto

func _ready():
	var desenho = get_node("AnimatedSprite")
	desenho.play("movendo")

func _process(delta):
	if (delta_fix >= 0.0005) :
		delta_fix = 0
		var degree_per_second = TAU / 8
		rotate(delta * degree_per_second)
	else:
		delta_fix += delta

func _on_Npc_body_entered(body):
	if(body.name == "Player"):
		queue_free()
		emit_signal("Npc_salvo")
