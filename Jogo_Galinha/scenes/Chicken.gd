extends Area2D

var delta_fix = 0

signal Start_timer
signal Start_timer2
signal Em_risco


var speed = 100
var velocity = Vector2(0,0)
var direction = 'L'
var chicken = "parada"
var mira = position.x


func _ready():
	parada()


func _process(delta):
	if chicken == "movendo":
		if direction == 'L':
			position += Vector2(-speed, 0)
		else:
			position += Vector2(speed, 0)

func parada():
	chicken = "parada"
	$AnimatedSprite.play(chicken + direction)
	$AnimatedSprite.stop()
	$CollisionShape2D.set_scale(Vector2(0.1,0.1))
	$CollisionShape2D.set_position(Vector2(0,0))	
	emit_signal("Start_timer")


func movendo():
	chicken = "movendo"
	$AnimatedSprite.play(chicken + direction)


func atacando():
	chicken = "atacando"
	$AnimatedSprite.play(chicken + direction)
	$CollisionShape2D.set_scale(Vector2(1,1))
	if direction == 'R':
		$CollisionShape2D.set_position(Vector2(200,375))
	else:
		$CollisionShape2D.set_position(Vector2(-200,375))
	emit_signal("Start_timer2")


func _on_Chicken_body_entered(body):
	if body.name == "Player":
		emit_signal("Em_risco")


func _on_Chicken_area_entered(area):
	if area.name.substr(1,3) == "Npc":
		atacando()


func _on_Gameplay_timeout(player, npcs):
	mira = player.x
	
	if npcs.size() > 0:
		var proximidade = abs(player.x - npcs[0].x)
		var nova_proximidade = proximidade
		mira = npcs[0].x
		
		for elemento in npcs:
			nova_proximidade = abs(player.x - elemento.x)
			if proximidade >= nova_proximidade:
				proximidade = nova_proximidade
				mira = elemento.x
	$CollisionShape2D.set_scale(Vector2(1,3))
	if mira > position.x:
		direction = 'R'
		$CollisionShape2D.set_position(Vector2(100,320))
	else:
		direction = 'L'
		$CollisionShape2D.set_position(Vector2(-100,320))
	movendo()


func _on_Gameplay_timeout2():
	parada()

